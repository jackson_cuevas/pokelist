package com.raywenderlich.pokelist.shared

import kotlinx.serialization.BinaryFormat

expect class Image

expect fun ByteArray.toNativeImage(): Image?

